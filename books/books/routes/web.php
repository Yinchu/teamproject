<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('/flight', "FlightController");

Route::resource('/departments', 'DepartmentController');

Route::resource('/', 'DirectorController');       //給予主任權限

Route::resource('/', 'StudentdataController');   //匯入學生資料

