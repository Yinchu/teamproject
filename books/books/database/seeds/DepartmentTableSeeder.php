<?php

use Illuminate\Database\Seeder;

class DepartmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $departments = [
            ['department' => 'lc', 'name' => "華語文學系"],
            ['department' => 'tic', 'name' => "臺灣語文與傳播學系"],
            ['department' => 'doct', 'name' => "文化觀光產業學系"],
            ['department' => 'ccdm', 'name' => "文化創意與數位行銷學系"],
            ['department' => 'bm', 'name' => "經營管理學系"],
            ['department' => 'id', 'name' => "工業設計學系"],
            ['department' => 'finance', 'name' => "財務金融學系"],
            ['department' => 'arch', 'name' => "建築學系"],
            ['department' => 'im', 'name' => "資訊管理學系"],
            ['department' => 'indigenous', 'name' => "原住民學士學位學程專班"],
            ['department' => 'che', 'name' => "化學工程學系"],
            ['department' => 'civil', 'name' => "土木與防災工程學系"],
            ['department' => 'she', 'name' => "環境與安全衛生工程學系"],
            ['department' => 'mse', 'name' => "材料科學工程學系"],
            ['department' => 'energy', 'name' => "能源工程學系"],
            ['department' => 'csie', 'name' => "資訊工程學系"],
            ['department' => 'deeweb', 'name' => "電子工程學系"],
            ['department' => 'mech', 'name' => "機械工程學系"],
            ['department' => 'eo', 'name' => "光電工程學系"],
            ['department' => 'ee', 'name' => "電機工程學系"]
        ];


        foreach ($departments as $department) {
            DB::table('departments')->insert($department);
        }
    }
}
