<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarkDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mark_datas', function (Blueprint $table) {
            $table->increments('id');
            $table->string("departmant");
            $table->string("Bitem");
            $table->string("Mitem");
            $table->string("Sitem");
            $table->longText("title");
            $table->integer("HighScore");
            $table->integer("lowScore");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mark_datas');
    }
}
