<?php

namespace App\Http\Controllers;
use function MongoDB\BSON\toJSON;
use App\Flight;
use Illuminate\Http\Request;
//給主任權限
class director extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //給主任的資料
        $users=Flight::where('position', 'DepartHead')->get();
        return view('');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //新增主任資料
        $datas = $request->all();
        $datas['login'] = 'dirhead ';
        Flight::create($datas);
        return redirect()->route('flight.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //抓權限
        $users=Flight::all();
        return json_encode($users);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //修改
        $flight = Flight::find($request->id);
        $flight->update($request->all());
        return redirect()->route('flight.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //刪除
        $flight = Flight::find($request->id);
        $flight->delete();
        return redirect()->route('flight.index');
    }
}
