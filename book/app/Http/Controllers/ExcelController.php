<?php

namespace App\Http\Controllers;

use DummyFullModelClass;
use App\excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ExcelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\excel  $excel
     * @return \Illuminate\Http\Response
     */
    public function index(excel $excel)
    {
        //
    }

    public function importflightsDepartOffice(Request $request){
        $this->validate($request, [
            'select_file' => 'required|mimes:xls,xlsx'
        ]);

        $path = $request->file('select_file')->getRealPath();

        $data = Excel::load($path)->get();

        if ($data->count() > 0){
            foreach ($data->toArray() as $row){
                $insert_data[] = array(
                    'account' => $row['account'],
                    'department' => $row['department'],
                    'login' => "offic",
                    'date' => $row['date'],
                );
            }
        }

        if(!empty($insert_data)){
            DB::table('flights')->insert($insert_data);
        }

        return back()->with('success','Excel Data Imported successfully');
    }
    //以上是insert 系辦權限 表單
    public function importflightsDepartHead(Request $request){
        $this->validate($request, [
            'select_file' => 'required|mimes:xls,xlsx'
        ]);

        $path = $request->file('select_file')->getRealPath();

        $data = Excel::load($path)->get();

        if ($data->count() > 0){
            foreach ($data->toArray() as $row){
                $insert_data[] = array(
                    'account' => $row['account'],
                    'department' => $row['department'],
                    'login' => "dirhead",
                    'date' => $row['date'],
                );
            }
        }

        if(!empty($insert_data)){
            DB::table('flights')->insert($insert_data);
        }

        return back()->with('success','Excel Data Imported successfully');
    }
    //以上是 insert 主任權限
    public function updateflightsDepartOffice(Request $request){
        $this->validate($request, [
            'select_file' => 'required|mimes:xls,xlsx'
        ]);

        $path = $request->file('select_file')->getRealPath();

        $data = Excel::load($path)->get();

        if ($data->count() > 0){
            foreach ($data->toArray() as $row){
                $insert_data[] = array(
                    'account' => $row['account'],
                    'department' => $row['department'],
                    'login' => "offic",
                    'date' => $row['date'],
                );
            }
        }

        if(!empty($insert_data)){
            DB::table('flights')->where('account',$insert_data)->delete();
            DB::table('flights')->insert($insert_data);
        }

        return back()->with('success','Excel Data Imported successfully');
    }
    //以上是 update 系辦權限
    public function updateflightsDepartHead(Request $request){
        $this->validate($request, [
            'select_file' => 'required|mimes:xls,xlsx'
        ]);

        $path = $request->file('select_file')->getRealPath();

        $data = Excel::load($path)->get();

        if ($data->count() > 0){
            foreach ($data->toArray() as $row){
                $insert_data[] = array(
                    'account' => $row['account'],
                    'department' => $row['department'],
                    'login' => "dirhead",
                    'date' => $row['date'],
                );
            }
        }

        if(!empty($insert_data)){
            DB::table('flights')->where('account',$insert_data)->delete();
            DB::table('flights')->insert($insert_data);
        }

        return back()->with('success','Excel Data Imported successfully');
    }
    //以上是 update 主任權限
    public function importstudents(Request $request){
        $this->validate($request, [
            'select_file' => 'required|mimes:xls,xlsx'
        ]);

        $path = $request->file('select_file')->getRealPath();

        $data = Excel::load($path)->get();

        if ($data->count() > 0){
            foreach ($data->toArray() as $row){
                $insert_data[] = array(
                    'Snum' => $row['Snum'],
                    'name' => $row['name'],
                    'department' => $row['department'],
                    'part' => $row['part'],
                    'date' => $row['date'],
                );
            }
        }

        if(!empty($insert_data)){
            DB::table('students')->insert($insert_data);
        }

        return back()->with('success','Excel Data Imported successfully');
    }
    //以上是匯入學生資料
    public function updatestudents(Request $request){
        $this->validate($request, [
            'select_file' => 'required|mimes:xls,xlsx'
        ]);

        $path = $request->file('select_file')->getRealPath();

        $data = Excel::load($path)->get();

        if ($data->count() > 0){
            foreach ($data->toArray() as $row){
                $insert_data[] = array(
                    'Snum' => $row['Snum'],
                    'name' => $row['name'],
                    'department' => $row['department'],
                    'part' => $row['part'],
                    'date' => $row['date'],
                );
            }
        }

        if(!empty($insert_data)){
            DB::table('students')->where('Snum',$insert_data)->delete();
            DB::table('students')->insert($insert_data);
        }

        return back()->with('success','Excel Data Imported successfully');
    }
    //以上是更新學生資料
    public function importflightsDepartTeacher(Request $request){
        $this->validate($request, [
            'select_file' => 'required|mimes:xls,xlsx'
        ]);

        $path = $request->file('select_file')->getRealPath();

        $data = Excel::load($path)->get();

        if ($data->count() > 0){
            foreach ($data->toArray() as $row){
                $insert_data[] = array(
                    'account' => $row['account'],
                    'department' => $row['department'],
                    'login' => "dratea",
                    'date' => $row['date'],
                );
            }
        }

        if(!empty($insert_data)){
            DB::table('flights')->insert($insert_data);
        }

        return back()->with('success','Excel Data Imported successfully');
    }
    //以上是給予老師權限
    public function updateflightsDepartTeacher(Request $request){
        $this->validate($request, [
            'select_file' => 'required|mimes:xls,xlsx'
        ]);

        $path = $request->file('select_file')->getRealPath();

        $data = Excel::load($path)->get();

        if ($data->count() > 0){
            foreach ($data->toArray() as $row){
                $insert_data[] = array(
                    'account' => $row['account'],
                    'department' => $row['department'],
                    'login' => "dratea",
                    'date' => $row['date'],
                );
            }
        }

        if(!empty($insert_data)){
            DB::table('flights')->where('account',$insert_data)->delete();
            DB::table('flights')->insert($insert_data);
        }

        return back()->with('success','Excel Data Imported successfully');
    }
    //以上是更新老師之權限
    public function importteacher(Request $request){
        $this->validate($request, [
            'select_file' => 'required|mimes:xls,xlsx'
        ]);

        $path = $request->file('select_file')->getRealPath();

        $data = Excel::load($path)->get();

        if ($data->count() > 0){
            foreach ($data->toArray() as $row){

                $insert_data[] = array(
                    'Tnum' => $row['Tnum'],
                    'name' => $row['name'],
                    'department' => $row['department'],
                    'Account' => $row['Account'],
                    'date' => $row['date'],
                );
            }
        }

        if(!empty($insert_data)){
            DB::table('teachers')->insert($insert_data);
        }

        return back()->with('success','Excel Data Imported successfully');
    }
    //以上是匯入書審老師資料
    public function updateteachers(Request $request){
        $this->validate($request, [
            'select_file' => 'required|mimes:xls,xlsx'
        ]);

        $path = $request->file('select_file')->getRealPath();

        $data = Excel::load($path)->get();

        if ($data->count() > 0){
            foreach ($data->toArray() as $row){
                $insert_data[] = array(
                    'Tnum' => $row['Tnum'],
                    'name' => $row['name'],
                    'department' => $row['department'],
                    'Account' => $row['Account'],
                    'date' => $row['date'],
                );
            }
        }

        if(!empty($insert_data)){
            DB::table('teachers')->where('Tnum',$insert_data)->delete();
            DB::table('teachers')->insert($insert_data);
        }

        return back()->with('success','Excel Data Imported successfully');
    }
    //以上是更新老師資料
    public function importmatches(Request $request){
        $this->validate($request, [
            'select_file' => 'required|mimes:xls,xlsx'
        ]);

        $path = $request->file('select_file')->getRealPath();

        $data = Excel::load($path)->get();

        if ($data->count() > 0){
            foreach ($data->toArray() as $row){

                $insert_data[] = array(
                    'Snum' => $row['Snum'],
                    'Tnum' => $row['Tnum'],
                    'choose' => $row['choose'],
                    'date' => $row['date'],
                );
            }
        }

        if(!empty($insert_data)){
            DB::table('matches')->insert($insert_data);
        }

        return back()->with('success','Excel Data Imported successfully');
    }
    //以上是匯入評分老師與學生配對
}
