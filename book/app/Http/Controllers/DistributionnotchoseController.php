<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Match;
use App\Teacher;
use App\Students;
use function MongoDB\BSON\toJSON;

class DistributionnotchoseController extends Controller
{

    
    //要檢查有沒有新的同學
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('welcome');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $department = "im";
        $users2=DB::table('matches')
                        ->join('students', 'matches.Snum', '=', 'students.Snum')
                         ->join('teachers', 'matches.Tnum', '=', 'teachers.Tnum')
                        ->select('matches.choose','matches.Snum','students.Snum', 'students.name','matches.Tnum','teachers.name') 
                        ->where('matches.choose',"=","0")
                        ->get();
                        
        return json_encode($users2);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        Match::where('id','=', $id)
          
          ->update(['choose' => 0]);
          return redirect()->route('welcome');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
}
