<!DOCTYPE html>
<html lang="en">
<head>
    <title>系務辦公室</title>
    <!-- Required meta tags -->
    <meta charset="utf-8"/>
    <meta
            name="viewport"
            content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />
    <link
            rel="icon"
            href="https://www.nuu.edu.tw/var/file/0/1000/msys_1000_9991812_60080.jpg"
            type="image/x-icon"
    />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap CSS -->
    <link
            rel="stylesheet"
            href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
            integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
            crossorigin="anonymous"
    />
    <link
            href="https://fonts.googleapis.com/css?family=Noto+Sans+TC&display=swap"
            rel="stylesheet"
    />
    <style>
        * {
            font-family: "Noto Sans TC", sans-serif;
        }
    </style>
</head>

<body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <a class="navbar-brand" href="/flight">系務辦公室</a>
    <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId" aria-controls="collapsibleNavId"
        aria-expanded="false" aria-label="Toggle navigation"><i class="fa fa-bars" aria-hidden="true"></i> </button>
    <div class="collapse navbar-collapse" id="collapsibleNavId">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            <li class="nav-item">
                <a class="nav-link" href="{{route('director.index')}}">建立主任資料</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">匯入學生資料</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">匯入評審委員資料</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="#">分配評分人員</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">評分項目與分數區間</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">查看所有老師評分細節</a>
            </li>
        </ul>
    </div>
</nav>

<!-- Button trigger modal -->

<!-- Modal -->

<section class="mt-5" id="app">
    @yield('content')
</section>

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.js"></script>
    <script
            src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"
    ></script>
    <script
            src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"
    ></script>
    <script
            src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"
    ></script>
@show
</body>
</html>
