<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('/flight', "FlightController");

Route::resource('/departments', 'DepartmentController');

Route::resource('/director','DirectorController');

Route::resource('/studentdata','StudentdataController');

Route::resource('/teacher','TeacherController');

Route::resource('/match','DistributionchoseController');

Route::resource('/nomatch','DistributionnotchoseController');

Route::resource('/matchnum','MatchNumController');

Route::get('users/{id}', function ($id) {

});
